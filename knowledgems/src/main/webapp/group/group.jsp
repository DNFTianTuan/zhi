<%--
  Created by IntelliJ IDEA.
  User: Creed
  Date: 2020/9/14
  Time: 13:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>
    <title>群组-知了[团队知识管理应用]</title>
</head>
<body>
<div class="re-main">
    <!-- START: Navbar -->
    <c:import url="../template/_navbar.jsp?menu=group"/>
    <!-- END: Navbar -->
    <div class="re-box-6 re-grey mt-85">
        <div class="container">
            <div class="row vertical-gap md-gap">
                <div class="col-lg-12">
                    <c:choose>
                        <c:when test="${userGroups==null || userGroups.size() == 0}">
                            <div class="resource-item re-box re-box-decorated">
                                <div class="resource-item-cont">
                                    <h2 class="h3 resource-item-title"><a href="single-post.html">你尚未加入任何群组，不会吧不会吧不会真有人这么懒吧</a></h2>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                        <div class="row vertical-gap dx-isotope-grid">
                            <c:forEach var="h" items="${userGroups}" varStatus="status">
                                <div class="col-12 col-md-6 col-lg-4 dx-isotope-grid-item" onclick="window.open('<%=request.getContextPath()%>/groupinfo?groupid=${h.groupId}','_self')" style="cursor:pointer">
                                    <div class="re-portfolio-item re-block-decorated">
                                        <div class="resource-item re-box re-box-decorated">
                                            <div class="resource-item-cont">
                                                <h3><a class="mb-30 text-dark mnt-7" href="<%=request.getContextPath()%>/groupinfo?groupid=${h.groupId}">${fn:substring(h.groupName,0,9)}${fn:length(h.groupName)>9?'...':''}</a></h3>
                                                <ul class="resource-item-info">
                                                    <li>群主：${h.leader}</li>
                                                    <li>成员数: ${h.memberNum}</li>
                                                    <li>创建时间: <fmt:formatDate value="${h.createTime}"  pattern="yyyy-MM-dd HH:mm"/></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                        </c:otherwise>
                    </c:choose>
                    <%--                <div class="dx-blog-item pt-0">--%>
                    <%--                    <a href="#" class="dx-btn dx-btn-lg dx-btn-grey dx-btn-block dx-btn-load" data-btn-loaded="Shown all posts">Load More Post</a>--%>
                    <%--                </div>--%>
                </div>
            </div>
        </div>
    </div>
    <!-- START: Footer -->
    <c:import url="../template/_footer.jsp"/>
    <!-- END: Footer -->
</div>
</body>
<!-- START: Scripts -->
<c:import url="../template/_include_js.jsp"/>
<!-- END: Scripts -->
</html>
