<%--
  Created by IntelliJ IDEA.
  User: 1
  Date: 2021/7/7
  Time: 22:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
    <title>首页-知了[团队知识管理应用]</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-reboot.css">
    <link rel="stylesheet" type="text/css" href="../css/mycss.css">
</head>

<body>
<div>
    <c:import url="_navbar.jsp?menu=index"/>
    <header class="dx-box-1">
        <div class="container">
            <div class="bg-image">
                <img src="../picture/bg-header-2.png" alt="">
                <div style="background-color: rgba(27, 27, 27, .8);"></div>
            </div>

            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                    <h1 class="mb-30 text-white text-center">团队知识管理</h1>
                    <div class="row">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2"></div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                            <form id="search-form" class="dx-form dx-form-group-inputs">
                                <%--输入框--%>
                                <input type="text" style="display: none;"/>
                                <input type="hidden" name="keyword" id="hkeyword"/>
                                <input id="query" type="text" name="queryStr" class="form-control dx-btn-rd" placeholder="在这里发现你想要的知识"/>
                                <button type="button" class="dx-btn dx-btn-lg dx-btn-r" onclick="searchData(1)"><a>搜&nbsp;&nbsp;索</a></button>
                            </form>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2"></div>
                        <%--                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2"--%>
                        <%--                             style="display: flex;/*实现垂直居中*/align-items: center;padding-left: 0px;">--%>
                        <%--                            <a href="../adsearch/adsearch" style="color: #ffffff">高级</a>--%>
                        <%--                        </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="container dx-box-6" style="background-color: #f9f9f9">
        <div class="row vertical-gap" >
            <div class="col-12 col-md-6 col-lg-4">
                <div class="dx-portfolio-item">
                    <div class="dx-portfolio-item-image" style="height: 340px">
                        <img src="../picture/index_0001.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 dx-isotope-grid-item">
                <div class="dx-portfolio-item">
                    <div class="dx-portfolio-item-image" style="height: 340px">
                        <img src="../picture/index_0002.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 dx-isotope-grid-item">
                <div class="dx-portfolio-item">
                    <div class="dx-portfolio-item-image" style="height: 340px">
                        <img src="../picture/index_0003.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div >
    <c:import url="_footer.jsp"/>
</div>

</body>
